# Enrich demo project #

The Database used : MySQL

The Schema management tool: Liquibase (https://www.liquibase.org/)


## The database intitalization sequence ##

```
CREATE DATABASE enrich;
USE enrich;
CREATE USER 'backend'@'%' IDENTIFIED BY 'enrich777';
GRANT ALL ON enrich.* TO 'backend'@'%';
```

## The database update command ##

```
liquibase --driver=com.mysql.jdbc.Driver --url=jdbc:mysql://localhost:3306/enrich?useSSL=false --username=backend --password=enrich777 --changeLogFile=db/master-1.0.xml update
```

## The project startup class ##

**com.wolandsoft.enrichdemo.be.Backend** 

The project assumes that the databse runs on the same host, but if you need to change that, plese edit the **src/main/resources/application.properties** file.

## Checkout API invocation example ## 

```
wget --method=POST --header='Content-Type: application/json' --body-data='
{
    "customer_id": 1,
    "vehicle_id": 1,
    "rented_at": "2020-01-01",
    "returned_at": "2020-10-11"
}' http://localhost:8008/api/rental/checkout
```

## Checkin API invocation example ## 

```
wget --method=PUT --header='Content-Type: application/json' --body-data='
{
	"customer_id": 1,
	"vehicle_id": 1,
	"charge": 100.00,
	"returned_at": "2020-04-09"
}' http://localhost:8008/api/rental/checkin
```
