package com.wolandsoft.enrichdemo.be.controller;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wolandsoft.enrichdemo.be.error.AppException;
import com.wolandsoft.enrichdemo.be.model.RentalHistory;
import com.wolandsoft.enrichdemo.be.model.dto.CheckinRequest;
import com.wolandsoft.enrichdemo.be.model.dto.CheckoutRequest;
import com.wolandsoft.enrichdemo.be.service.RentalHistoryService;

@CrossOrigin
@RestController
@RequestMapping("/rental")
public class RentalController {
	private static final Logger log = LoggerFactory.getLogger(RentalHistory.class);

	@Autowired
	private RentalHistoryService rentalService;

	@RequestMapping(value = "checkout", method = RequestMethod.POST)
	public ResponseEntity<?> checkout(@RequestBody CheckoutRequest data, HttpServletRequest request) {
		int recId = rentalService.checkout(data);
		try {
			return ResponseEntity.created(new URI(request.getRequestURI() + "/" + recId)).build();
		} catch (URISyntaxException e) {
			throw new AppException(e);
		}
	}

	@RequestMapping(value = "checkin", method = RequestMethod.PUT)
	public ResponseEntity<?> checkin(@RequestBody CheckinRequest data) {
		rentalService.checkin(data);
		return ResponseEntity.noContent().build();
	}
}
