package com.wolandsoft.enrichdemo.be.error;

import org.springframework.http.HttpStatus;

public class AppException extends RuntimeException {
	private static final long serialVersionUID = -5394618092790214625L;

	private ErrorCode error;
	private HttpStatus status;

	public AppException() {
		this(ErrorCode.UNKNOWN, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public AppException(Throwable thr) {
		super(thr.getMessage(), thr);
		this.error = ErrorCode.UNKNOWN;
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
	}

	public AppException(ErrorCode error) {
		this(error, HttpStatus.BAD_REQUEST);
	}

	public AppException(ErrorCode error, HttpStatus status) {
		super(error.name());
		this.error = error;
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public HttpStatus getHttpStatus() {
		return status;
	}

	public ErrorCode getErrorCode() {
		return error;
	}
}
