package com.wolandsoft.enrichdemo.be.error;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {

	public Error(AppException wex) {
		this(wex.getErrorCode().getCode(), wex.getErrorCode().name());
	}

	public Error(int code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	@JsonProperty("code")
	private int code;

	@JsonProperty("name")
	private String name;

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
