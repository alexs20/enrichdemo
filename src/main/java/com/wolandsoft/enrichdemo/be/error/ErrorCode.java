package com.wolandsoft.enrichdemo.be.error;

public enum ErrorCode {

	UNKNOWN(0),
	REJECTED(1),
	INVALID_CUSTOMER(2),
	INVALID_VEHICLE(3),
	INVALID_RENTAL_DATES(4),
	INVALID_RENTAL(5),
	DUPLICATE_RENTAL(6),
	INVALID_CHARGE(7)
	;
	
	int code;
	
	private ErrorCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
	
	
}
