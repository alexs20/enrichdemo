package com.wolandsoft.enrichdemo.be.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wolandsoft.enrichdemo.be.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Integer> {

}
