package com.wolandsoft.enrichdemo.be.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wolandsoft.enrichdemo.be.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

}
