package com.wolandsoft.enrichdemo.be.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wolandsoft.enrichdemo.be.model.RentalHistory;

public interface RentalHistoryRepository extends JpaRepository<RentalHistory, Integer> {

	List<RentalHistory> findByCustomerIdAndVehicleIdAndCharge(int customerId, int vehicleId, float charge);
}
