package com.wolandsoft.enrichdemo.be.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wolandsoft.enrichdemo.be.model.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {

}
