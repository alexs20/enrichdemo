package com.wolandsoft.enrichdemo.be.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "company")
public class Company implements Serializable {
	private static final long serialVersionUID = 8992927764348962282L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Basic
	@Column(name = "name")
	private String name;

	@Basic
	@Column(name = "address")
	private String address;

    @OneToMany(mappedBy = "company")
    private Set<Customer> customers = new HashSet<Customer>(0);

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Company that = (Company) o;
		//@formatter:off
		return Objects.equals(id, that.id)
				&& Objects.equals(name, that.name)
				&& Objects.equals(address, that.address)
				;
		//@formatter:on
	}

	@Override
	public int hashCode() {
		//@formatter:off
		return Objects.hash(
				id,
				name,
				address);
		//@formatter:on
	}

	@Override
	public String toString() {
		//@formatter:off
        return getClass().getSimpleName() + "{" + String.join(", ",
                "id=" + id,
                "name=" + name,
                "address=" + address
                ) + "}";
      //@formatter:on
	}

}
