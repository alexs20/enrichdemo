package com.wolandsoft.enrichdemo.be.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {
	private static final long serialVersionUID = -5843282600455327933L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Basic
	@Column(name = "first_name")
	private String firstName;

	@Basic
	@Column(name = "last_name")
	private String lastName;

	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "driver_license_expired_at")
	private Date driverLicenseExpiredAt;
	
	@Basic
	@Column(name = "phone_number")
	private String phoneNumber;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="company_id")
	private Company company;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDriverLicenseExpiredAt() {
		return driverLicenseExpiredAt;
	}

	public void setDriverLicenseExpiredAt(Date driverLicenseExpiredAt) {
		this.driverLicenseExpiredAt = driverLicenseExpiredAt;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Customer that = (Customer) o;
		//@formatter:off
		return Objects.equals(id, that.id)
				&& Objects.equals(firstName, that.firstName)
				&& Objects.equals(lastName, that.lastName)
				&& Objects.equals(driverLicenseExpiredAt, that.driverLicenseExpiredAt)
				&& Objects.equals(company, that.company)
				;
		//@formatter:on
	}

	@Override
	public int hashCode() {
		//@formatter:off
		return Objects.hash(
				id,
				firstName,
				lastName,
				driverLicenseExpiredAt,
				company);
		//@formatter:on
	}

	@Override
	public String toString() {
		//@formatter:off
        return getClass().getSimpleName() + "{" + String.join(", ",
                "id=" + id,
                "firstName=" + firstName,
                "lastName=" + lastName,
                "driverLicenseExpiredAt=" + driverLicenseExpiredAt,
                "company=" + company
                ) + "}";
      //@formatter:on
	}

}
