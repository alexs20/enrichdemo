package com.wolandsoft.enrichdemo.be.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "rental_history")
public class RentalHistory implements Serializable {
	private static final long serialVersionUID = -6720349553319669715L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Basic
	@Column(name = "rented_at")
	@Temporal(TemporalType.DATE)
	private Date rentedAt;

	@Basic
	@Column(name = "returned_at")
	@Temporal(TemporalType.DATE)
	private Date returnedAt;

	@Basic
	@Column(name = "charge")
	private Float charge;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="customer_id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="vehicle_id")
	private Vehicle vehicle;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getRentedAt() {
		return rentedAt;
	}

	public void setRentedAt(Date rentedAt) {
		this.rentedAt = rentedAt;
	}

	public Date getReturnedAt() {
		return returnedAt;
	}

	public void setReturnedAt(Date returnedAt) {
		this.returnedAt = returnedAt;
	}

	public Float getCharge() {
		return charge;
	}

	public void setCharge(Float charge) {
		this.charge = charge;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		RentalHistory that = (RentalHistory) o;
		//@formatter:off
		return Objects.equals(id, that.id)
				&& Objects.equals(rentedAt, that.rentedAt)
				&& Objects.equals(returnedAt, that.returnedAt)
				&& Objects.equals(charge, that.charge)
				&& Objects.equals(customer, that.customer)
				&& Objects.equals(vehicle, that.vehicle)
				;
		//@formatter:on
	}

	@Override
	public int hashCode() {
		//@formatter:off
		return Objects.hash(
				id,
				rentedAt,
				returnedAt,
				charge,
				customer,
				vehicle);
		//@formatter:on
	}

	@Override
	public String toString() {
		//@formatter:off
        return getClass().getSimpleName() + "{" + String.join(", ",
                "id=" + id,
                "rentedAt=" + rentedAt,
                "returnedAt=" + returnedAt,
                "charge=" + charge,
                "customer=" + customer,
                "vehicle=" + vehicle
                ) + "}";
      //@formatter:on
	}

}
