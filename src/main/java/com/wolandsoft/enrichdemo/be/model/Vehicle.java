package com.wolandsoft.enrichdemo.be.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wolandsoft.enrichdemo.be.model.enums.VehicleStatus;

@Entity
@Table(name = "vehicle")
public class Vehicle implements Serializable {
	private static final long serialVersionUID = 310007213906425164L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private Integer id;

	@Basic
	@Column(name = "status")
	@JsonProperty("status")
	private String status;

	@Basic
	@Column(name = "odometer")
	@JsonProperty("odometer")
	private String odometer;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public VehicleStatus getVehicleStatus() {
		return VehicleStatus.valueOfCode(status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setVehicleStatus(VehicleStatus status) {
		this.status = status.getCode();
	}

	public String getOdometer() {
		return odometer;
	}

	public void setOdometer(String odometer) {
		this.odometer = odometer;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Vehicle that = (Vehicle) o;
		//@formatter:off
		return Objects.equals(id, that.id)
				&& Objects.equals(status, that.status)
				&& Objects.equals(odometer, that.odometer)
				;
		//@formatter:on
	}

	@Override
	public int hashCode() {
		//@formatter:off
		return Objects.hash(
				id,
				status,
				odometer);
		//@formatter:on
	}

	@Override
	public String toString() {
		//@formatter:off
        return getClass().getSimpleName() + "{" + String.join(", ",
                "id=" + id,
                "status=" + status,
                "odometer=" + odometer
                ) + "}";
      //@formatter:on
	}

}
