package com.wolandsoft.enrichdemo.be.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckinRequest {
	@JsonProperty("customer_id")
	private int customerId;

	@JsonProperty("vehicle_id")
	private int vehicleId;

	@JsonProperty("returned_at")
	private Date returnedAt;

	@JsonProperty("charge")
	private float charge;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Date getReturnedAt() {
		return returnedAt;
	}

	public void setReturnedAt(Date returnedAt) {
		this.returnedAt = returnedAt;
	}

	public float getCharge() {
		return charge;
	}

	public void setCharge(float charge) {
		this.charge = charge;
	}

}
