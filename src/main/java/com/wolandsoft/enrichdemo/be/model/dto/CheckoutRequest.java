package com.wolandsoft.enrichdemo.be.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckoutRequest {
	@JsonProperty("customer_id")
	private int customerId;

	@JsonProperty("vehicle_id")
	private int vehicleId;

	@JsonProperty("rented_at")
	private Date rentedAt;

	@JsonProperty("returned_at")
	private Date returnedAt;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Date getRentedAt() {
		return rentedAt;
	}

	public void setRentedAt(Date rentedAt) {
		this.rentedAt = rentedAt;
	}

	public Date getReturnedAt() {
		return returnedAt;
	}

	public void setReturnedAt(Date returnedAt) {
		this.returnedAt = returnedAt;
	}

}
