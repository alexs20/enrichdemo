package com.wolandsoft.enrichdemo.be.model.enums;

public enum VehicleStatus {
	
	AVAILABLE("available"),
	RENTED("rented"),
	OUT_OF_SERVICE("out-of-service"),
	IDLE("idle");

	private String code;

	VehicleStatus(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public static VehicleStatus valueOfCode(String code) {
		if (code == null) {
			throw new NullPointerException();
		}
		for (VehicleStatus state : VehicleStatus.values()) {
			if (state.getCode().equals(code)) {
				return state;
			}
		}
		throw new IllegalArgumentException();
	}
}
