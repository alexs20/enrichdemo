package com.wolandsoft.enrichdemo.be.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wolandsoft.enrichdemo.be.error.AppException;
import com.wolandsoft.enrichdemo.be.error.ErrorCode;
import com.wolandsoft.enrichdemo.be.jpa.CustomerRepository;
import com.wolandsoft.enrichdemo.be.jpa.RentalHistoryRepository;
import com.wolandsoft.enrichdemo.be.jpa.VehicleRepository;
import com.wolandsoft.enrichdemo.be.model.Customer;
import com.wolandsoft.enrichdemo.be.model.RentalHistory;
import com.wolandsoft.enrichdemo.be.model.Vehicle;
import com.wolandsoft.enrichdemo.be.model.dto.CheckinRequest;
import com.wolandsoft.enrichdemo.be.model.dto.CheckoutRequest;
import com.wolandsoft.enrichdemo.be.model.enums.VehicleStatus;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class RentalHistoryService {
	private static final Logger log = LoggerFactory.getLogger(RentalHistoryService.class);

	@Autowired
	private CustomerRepository customerRep;
	@Autowired
	private VehicleRepository vehicleRep;
	@Autowired
	private RentalHistoryRepository rentalRep;

	@Transactional
	public int checkout(CheckoutRequest data) {
		Optional<Customer> optCustomer = customerRep.findById(data.getCustomerId());
		if (!optCustomer.isPresent()) {
			throw new AppException(ErrorCode.INVALID_CUSTOMER);
		}
		Customer customer = optCustomer.get();
		if (customer.getDriverLicenseExpiredAt().before(new Date())) {
			throw new AppException(ErrorCode.INVALID_CUSTOMER);
		}
		
		Optional<Vehicle> optVehicle = vehicleRep.findById(data.getVehicleId());
		if (!optVehicle.isPresent()) {
			throw new AppException(ErrorCode.INVALID_VEHICLE);
		}
		Vehicle vehicle = optVehicle.get();
		if (vehicle.getVehicleStatus() != VehicleStatus.AVAILABLE) {
			throw new AppException(ErrorCode.INVALID_VEHICLE);
		}

		if (data.getRentedAt() == null || data.getReturnedAt() == null || 
				data.getRentedAt().after(new Date()) || data.getRentedAt().after(data.getReturnedAt())) {
			throw new AppException(ErrorCode.INVALID_RENTAL_DATES);
		}
		
		List<RentalHistory> rentalRecs = rentalRep.findByCustomerIdAndVehicleIdAndCharge(data.getCustomerId(), data.getVehicleId(), 0F);
		if (!rentalRecs.isEmpty()) {
			throw new AppException(ErrorCode.DUPLICATE_RENTAL);
		}
		
		vehicle.setVehicleStatus(VehicleStatus.RENTED);
		
		RentalHistory historyRec = new RentalHistory();
		historyRec.setCustomer(customer);
		historyRec.setVehicle(vehicle);
		historyRec.setRentedAt(data.getRentedAt());
		historyRec.setReturnedAt(data.getReturnedAt());
		historyRec.setCharge(0F);
		
		RentalHistory ret = rentalRep.saveAndFlush(historyRec);
		
		return ret.getId();
	}

	@Transactional
	public void checkin(CheckinRequest data) {
		Optional<Customer> optCustomer = customerRep.findById(data.getCustomerId());
		if (!optCustomer.isPresent()) {
			throw new AppException(ErrorCode.INVALID_CUSTOMER);
		}
		
		Optional<Vehicle> optVehicle = vehicleRep.findById(data.getVehicleId());
		if (!optVehicle.isPresent()) {
			throw new AppException(ErrorCode.INVALID_VEHICLE);
		}

		List<RentalHistory> rentalRecs = rentalRep.findByCustomerIdAndVehicleIdAndCharge(data.getCustomerId(), data.getVehicleId(), 0F);
		if (rentalRecs.isEmpty()) {
			throw new AppException(ErrorCode.INVALID_RENTAL);
		}
		RentalHistory rental = rentalRecs.get(0);
		if (data.getCharge() <= 0F) {
			throw new AppException(ErrorCode.INVALID_CHARGE);
		}
		if (data.getReturnedAt() == null || data.getReturnedAt().after(new Date()) || rental.getRentedAt().after(data.getReturnedAt())) {
			throw new AppException(ErrorCode.INVALID_RENTAL_DATES);
		}

		rental.getVehicle().setVehicleStatus(VehicleStatus.AVAILABLE);

		rental.setCharge(data.getCharge());
		rental.setReturnedAt(data.getReturnedAt());
		
		rentalRep.saveAndFlush(rental);

	}
}
